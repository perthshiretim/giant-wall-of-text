# Giant Wall Of Text

This is a theme for the [(U)EFI rEFInd boot manager](https://sourceforge.net/projects/refind/) bootmanager.  The theme
is a little cheeky twist of irony on what people consider _the_ graphical bootmanager.  All artwork, apart from the
background, is taken from the popular [neofetch](https://github.com/dylanaraps/neofetch), and covered by the MIT
licence.  The Manjaro logo was slightly altered, by swapping the block characters for 'm's.  The Apple logo was slightly
altered by changing the colour of the top two bands of red to orange.

![ASCII controls](https://i.imgur.com/F9p0jnT.jpg)

_Giant Wall Of Text with the ASCII controls ..._

![Unicode controls](https://i.imgur.com/Oc9FKFh.jpg)

_... and with the prettier, but less themey, Unicode controls._

## Installation

As rEFInd has no means for elements to be placed at a given position, and trying to line up text with text requires
accurate positioning, you will need to include the slightly amusing bash script, `install.bash`.  The script will ask
you for a resolution which your UEFI firmware can use.  Note that along with `sed` and `awk`, you will need to have
`imagemagick` installed.

![Installer](https://i.imgur.com/GY4KHCv.jpg)

_The slightly amusing installer --- after colouring "GIANT WALL", I learned to enter escape codes starting from the end of the line_

## Licence Information

The artwork from [neofetch](https://github.com/dylanaraps/neofetch) is covered by the MIT licence, a copy of which is
included in this directory.  All other work is my own, and is licensed under [Creative Commons Attribution-ShareAlike
4.0 International](https://creativecommons.org/licenses/by/4.0) with required attribution given to myself, Tim Clarke.
