#!/bin/bash

function error
{
    printf "\e[91m%s\e[0m%s\n\n" "Error: " "$1"
}


function exitwith
{
    error "$1"
    exit 1
}


function readpattern
{
    local __prompt=$1
    local __client_var=$2
    local __pattern=$3
    local __pattern_description=$4

    while [ true ]
    do
        printf "\e[95m%s\e[0m" "$__prompt: "
        local __user_input
        read __user_input

        if [[ $__user_input =~ $__pattern ]]
        then
            eval $__client_var="'$__user_input'"
            break
        else
            error "value must be $__pattern_description"
        fi
    done
}


function readnumeric
{
    readpattern "$1" $2 "^[0-9]+$" "a whole number"
}


function trimfile
{
    rnd=$(( RANDOM % 1000 ))
    tac "$1" | sed -e '/[^[:blank:]]/,$!d' | tac > "$1.tmp.$rnd"
    mv "$1.tmp.$rnd" "$1"
}


src_dir=src
dest_dir=giant-wall-of-text
gwot="\e[95mG\e[94mi\e[93ma\e[96mn\e[92mt \e[96mW\e[93ma\e[95ml\e[94ml \e[96mO\e[92mf \e[95mT\e[93me\e[94mx\e[95mt"


printf "\e[91m_|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|\n"
printf "___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__\n"
printf "_|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|\n"
printf "___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__\n"
printf "_|___ \e[95m________\e[94m.___   \e[93m_____    \e[96m_______\e[92m___________  \e[96m__      __  \e[93m_____  \e[95m.____    \e[94m._____\e[91m_|___|\n"
printf "___| \e[95m/  _____/\e[94m|   | \e[93m/  _  \   \e[96m\      \\\\\e[92m__    ___/ \e[96m/  \    /  \\\\\e[93m/  _  \ \e[95m|    |   \e[94m|    |\e[91m___|__\n"
printf "_|_ \e[95m/   \  ___\e[94m|   |\e[93m/  /_\  \  \e[96m/   |   \\\\\e[92m|    |    \e[96m\   \/\/   /  \e[93m/_\  \\\\\e[95m|    |   \e[94m|    |\e[91m_|___|\n"
printf "___|\e[95m\    \_\  \   \e[93m/    |    \\\\\e[96m/    |    \    \e[92m| \e[91m_|_ \e[96m\        /    \e[93m|    \    \e[95m|___\e[94m|    |___\e[91m|__\n"
printf "_|___\e[95m\______  /\e[94m___\e[93m\____|__  /\e[96m\____|__  /\e[92m____| \e[91m___|  \e[96m\__/\ /\e[93m\____|__  /\e[95m_______ \\\\\e[94m_______ \\\\\e[91m_|\n"
printf "___|_       \e[95m\/            \e[93m\/         \e[96m\/      \e[91m__|_       \e[96m\/         \e[93m\/        \e[95m\/       \e[94m\/\e[91m__\n"
printf "_|_ \e[96m________  \e[92m___________ \e[95m___________\e[93m___________\e[94m____  ___\e[92m____________ \e[91m___|___|___|___|___|\n"
printf "___|\e[96m\_____  \ \e[92m\_   _____/ \e[95m\__    ___/\e[93m\_   _____/\e[94m\   \/  /\e[92m\__    ___/\e[91m___|___|___|___|___|__\n"
printf "_|__ \e[96m/   |   \ \e[92m|    __)  \e[91m__ \e[95m|    |    \e[93m|    __)_  \e[94m\     /   \e[92m|    |\e[91m ___|___|___|___|___|___|\n"
printf "___|\e[96m/    |    \\\\\e[92m|     \ \e[91m|___ \e[95m|    |    \e[93m|        \ \e[94m/     \   \e[92m|    |\e[91m__|___|___|___|___|___|__\n"
printf "_|_ \e[96m\_______  /\e[92m\___  / \e[91m__|_ \e[95m|____|   \e[93m/_______  /\e[94m/___/\  \  \e[92m|____|\e[91m ___|___|___|___|___|___|\n"
printf "___|        \e[96m\/     \e[92m\/ \e[91m_|__                   \e[93m\/       \e[94m\_/\e[91m__|___|___|___|___|___|___|___|__\n"
printf "_|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|\n"
printf "___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__\n"
printf "_|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|\n"
printf "___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__\n"
printf "_|__                                                                                   __|\n"
printf "___| \e[97mThis script will prepare the $gwot \e[97mrEFInd theme.  You should already \e[91m|__\n"
printf "_|__ \e[97mknow a screen resolution \e[97mthat your computer firmware will display.  Along with \e[95msed \e[91m_|\n"
printf "___| \e[97mand \e[95mawk\e[97m, your system must have \e[95mImageMagick\e[97m installed.  With these programs, and \e[91m__|__\n"
printf "_|__ \e[97mknowledge of your screen resolution, this script will prepare the files located in \e[91m_|\n"
printf "___| \e[97mthe directory named \e[95msrc\e[97m, and place them in the \e[95mgiant-wall-of-text\e[97m directory. \e[91m_|___|__\n"
printf "_|__                                                                                _|___|\n"
printf "___| \e[97mThe theme is designed to have ASCII controls (selectors and arrows).  However, a \e[91m_|__\n"
printf "_|__ \e[97mUnicode version of these controls is included for those who prefer a slightly \e[91m__|___|\n"
printf "___| \e[97mprettier theme.  This script will give you the choice of which you prefer. \e[91m___|___|__\n"
printf "_|__                                                                           __|___|___|\n"
printf "___| \e[97mIf you run this script as \e[95mroot\e[97m, and it is able to find the rEFInd installation \e[91m___|__\n"
printf "_|__ \e[97mdirectory, then you will be asked if you wish for the rEFInd configuration file to \e[91m_|\n"
printf "___| \e[97mbe updated, and the generated \e[95mG\e[94mi\e[93ma\e[96mn\e[92mt \e[96mW\e[93ma\e[95ml\e[94ml \e[96mO\e[92mf \e[95mT\e[93me\e[94mx\e[92mt \e[97mfiles to be moved into the themes \e[91m__\n"
printf "_|__ \e[97mdirectory.  This will have to be done manually if the script is not run as root, or \e[91m|\n"
printf "___| \e[97mif the rEFInd folder cannot be found.                                             \e[91m|__\n"
printf "_|__                                          ___|___|___|___|___|___|___|___|___|___|___|\n"
printf "___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__\n"
printf "_|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|\n"
printf "___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__\e[0m\n\n"


if [ ! -d "$src_dir" ]; then exitwith "this script must be run from its own directory"
elif [ ! -x "$(command -v convert)" ]; then exitwith "imagemagick must be installed"
elif [ ! -x "$(command -v sed)" ]; then exitwith "sed must be installed"
elif [ ! -x "$(command -v awk)" ]; then exitwith "awk must be installed"
fi

if [[ -d "$dest_dir" ]]; then rm -Rf "$dest_dir"; fi
mkdir -p "$dest_dir"/img


readnumeric "Screen resolution width" "resolution_width"
echo
readnumeric "Screen resolution height" "resolution_height"
echo

convert "$src_dir"/img/banner.png -crop ${resolution_width}x${resolution_height}+0+0 "$dest_dir"/img/banner.png
sed "/^resolution/ s/$/ $resolution_width $resolution_height/" "$src_dir"/theme.conf > "$dest_dir"/theme.conf


eval `echo - | awk \
    -v resolution_width=$resolution_width \
    -v resolution_height=$resolution_height \
    '
        function ceil( value )
        {
            return int(( value == int( value )) ? value : ( int( value ) + 1 ))
        }

        function round( value )
        {
            return int( sprintf( "%.0f", value ))
        }

        function PaddingTop( i_y_coord )
        {
            font_height = 16
            rows = ceil( i_y_coord / font_height )
            return ( round(( rows * font_height ) - i_y_coord ) % font_height )
        }

        function PaddingLeft( i_x_coord )
        {
            font_width = 8
            cols = ceil( i_x_coord / font_width )
            return ( round(( cols * font_width ) - i_x_coord ) % font_width )
        }

        {
            horizontal_spacing = 8
            vertical_spacing = 16

            os_size = 384
            os_with_bg_size = int(( os_size * 9 ) / 8 )
            os_with_bg_offset = int(( os_with_bg_size - os_size ) / 2 )
            os_x_coord = int(( resolution_width - os_with_bg_size ) / 2 )
            os_y_coord = int(( resolution_height / 2 ) - ( os_with_bg_size / 2 ))
            printf( "os_padding=%dx%d;", PaddingLeft( os_x_coord + os_with_bg_offset ),
                PaddingTop( os_y_coord + os_with_bg_offset ))
            printf( "selector_os_padding=%dx%d;", PaddingLeft( os_x_coord ), PaddingTop( os_y_coord ))

            func_size = 90
            func_with_bg_size = int(( func_size * 4 ) / 3 )
            func_with_bg_offset = int(( func_with_bg_size - func_size ) / 2 )
            func_x_coord = int(( resolution_width - func_with_bg_size ) / 2 )
            func_y_coord = ( os_y_coord + os_with_bg_size + vertical_spacing )
            printf( "func_padding=%dx%d;", PaddingLeft( func_x_coord + func_with_bg_offset ),
                PaddingTop( func_y_coord + func_with_bg_offset ))
            printf( "selector_func_padding=%dx%d;", PaddingLeft( func_x_coord ), PaddingTop( func_y_coord ))

            vol_size = int( os_size / 4 )
            vol_offset = 8
            printf( "vol_padding=%dx%d;",
                PaddingLeft( os_x_coord + os_with_bg_offset + os_size - vol_size - vol_offset ),
                PaddingTop( os_y_coord + os_with_bg_offset + os_size - vol_size - vol_offset ))

            arrow_size = func_size
            arrow_padding_top = PaddingTop( int( os_y_coord + ( os_with_bg_size / 2 ) - ( arrow_size / 2 )))
            printf( "arrow_left_padding=%dx%d;", PaddingLeft( os_x_coord - horizontal_spacing - arrow_size ),
                arrow_padding_top )
            printf( "arrow_right_padding=%dx%d;",
                PaddingLeft( int(( resolution_width + os_with_bg_size + horizontal_spacing ) / 2 ) + horizontal_spacing ),
                arrow_padding_top )
        }
    '`


printf "\e[95mGenerating images to \e[92m`pwd`/$dest_dir/\e[95m:\e[0m "
groups=(os selector_os func selector_func vol arrow_left arrow_right)

for group in ${groups[@]}
do
    eval padding=\$${group}_padding
    src_files=(`find "$src_dir" -name "*${group}*\.png"`)

    for src_file in ${src_files[@]}
    do
        printf "."
        dest_file="$dest_dir"/img/"`basename \"$src_file\"`"

        if [ `identify -ping -format '%k' "$src_file"` = 1 ]
        then
            cp "$src_file" "$dest_file"
        else
            convert "$src_file" \
                -channel RGBA -fuzz 99% -fill transparent -opaque none \
                -gravity North-West -background transparent -splice $padding \
                -gravity South-East -chop $padding \
                "$dest_file"
        fi
    done
done

echo
echo


readpattern "`echo -e \"[\e[97ma\e[95m]scii controls, or [\e[97mu\e[95m]nicode\"`" "controls" "^[au]$" "'a' or 'u'"
echo
if [ $controls = a ]; then controls=ascii; else controls=unicode; fi
for filename in "$dest_dir"/img/${controls}_* ; do mv "$filename" "${filename/${controls}_/}" ; done


refind_directories=\
( \
    "/boot/efi/EFI/refind" \
    "/boot/EFI/refind" \
    "/efi/EFI/refind" \
    "/boot/EFI/BOOT" \
)

for dirname in ${refind_directories[@]}
do
    if [ -d "$dirname" ] && [ -f "$dirname"/refind.conf ]
    then
        refind_dir="$dirname"
        break
    fi
done

if [ ! -z "$refind_dir" ]
then
    if [ "$UID" = 0 ]
    then
        readpattern "`echo -e \"Install to \e[92m$refind_dir/themes/$dest_dir/\e[95m ([\e[97my\e[95m]es or [\e[97mn\e[95m]o)\"`" do_install "^[yn]$" "'y' or 'n'"

        if [ $do_install = y ]
        then
            if [[ -d "$refind_dir"/themes/"$dest_dir" ]]
            then
                rm -Rf "$refind_dir"/themes/"$dest_dir"
            elif [ ! -d "$refind_dir"/themes ]
            then
                mkdir "$refind_dir"/themes
            fi

            cp -r "$dest_dir" "$refind_dir"/themes/"$dest_dir"

            sed -e "/^include/d" -i "$refind_dir"/refind.conf
            trimfile "$refind_dir"/refind.conf
            echo -e "\ninclude themes/giant-wall-of-text/theme.conf\n" >> "$refind_dir"/refind.conf

            printf "\n\e[95mInstalled to \e[92m$refind_dir/themes/$dest_dir\e[95m\n"
        fi
    fi

    if [ $do_install != y ]
    then
        printf "\n\e[95mTo install $gwot, copy \e[92m$dest_dir/\e[95m to \e[92m$refind_dir/themes/\e[95m, and\n"
        printf "modify \e[92m$refind_dir/refind.conf\e[95m to \e[92minclude\e[95m it.  This will require root permissions.\n"
    fi
else
    printf "\e[95mTo install $gwot, copy \e[92m`pwd`/$dest_dir/\e[95m to a \e[92mthemes\e[95m\n"
    printf "subdirectory of the rEFInd installation directory, and modify the file \e[92mrefind.conf\e[95m (also\n"
    printf "located in the rEFInd directory) to \e[92minclude\e[95m it.  This will require root permissions.\n"
fi

printf "\nThank you for downloading $gwot, I hope you like it.  And remember, paragraphs\n"
printf "make the world a happier place.\n"
printf "    -- \e[96mPerthshireTim\e[0m\n\n"